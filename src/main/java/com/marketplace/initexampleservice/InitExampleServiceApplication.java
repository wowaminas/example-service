package com.marketplace.initexampleservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class InitExampleServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(InitExampleServiceApplication.class, args);
	}

}
